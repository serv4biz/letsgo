package letsgo

import (
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
)

// AddModuleFunc is add tag function
func AddModuleFunc(pathTag string, handler func(c *Context, params *jsons.Object) string) {
	MapModuleFunc[strings.TrimSpace(strings.ToLower(pathTag))] = handler
}
