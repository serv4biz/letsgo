package letsgo

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/logs"
)

func ProcessWithCache(c *Context, pathfile string) error {
	MutexMapCache.RLock()
	cache, ok := MapCache[pathfile]
	MutexMapCache.RUnlock()

	if ok {
		SetMimeType(c)
		MutexMapCache.RLock()
		for key, value := range cache.Header {
			c.Response.Header().Add(key, value[0])
		}
		MutexMapCache.RUnlock()

		err := c.Write(http.StatusOK, cache.Data)
		if logs.Error(err, true) {
			return err
		}
		return nil
	}

	if files.ExistFile(pathfile) {
		if files.IsFile(pathfile) {
			buff, err := files.ReadFile(pathfile)
			if logs.Error(err, true) {
				c.Error(http.StatusText(http.StatusNotFound), http.StatusNotFound)
				return err
			}

			cache = new(Cache)
			if IsTagType(pathfile) && !IsSkipTag(pathfile) {
				buff = []byte(RequestTag(c, string(buff)))
			}
			cache.Data = buff

			SetMimeType(c)
			cache.Header = http.Header{}
			for key, value := range c.Response.Header() {
				cache.Header.Add(key, value[0])
			}

			err = c.Write(http.StatusOK, cache.Data)
			if logs.Error(err, true) {
				return err
			}

			MutexMapCache.Lock()
			MapCache[pathfile] = cache
			MutexMapCache.Unlock()

			return nil
		}
	}

	if IsIndex(pathfile) {
		return Index(c)
	}

	c.Error(http.StatusText(http.StatusNotFound), http.StatusNotFound)
	return nil
}

func ProcessNoCache(c *Context, pathfile string) error {
	if files.ExistFile(pathfile) {
		if files.IsFile(pathfile) {
			buff, err := files.ReadFile(pathfile)
			if logs.Error(err, true) {
				c.Error(http.StatusText(http.StatusNotFound), http.StatusNotFound)
				return err
			}

			SetMimeType(c)
			if IsTagType(pathfile) && !IsSkipTag(pathfile) {
				buff = []byte(RequestTag(c, string(buff)))
			}
			err = c.Write(http.StatusOK, buff)
			if logs.Error(err, true) {
				return err
			}
			return nil
		}
	}
	if IsIndex(pathfile) {
		return Index(c)
	}

	c.Error(http.StatusText(http.StatusNotFound), http.StatusNotFound)
	return nil
}

// Process is main processing
func Process(c *Context) error {
	pathfile := fmt.Sprint(APP_DIR, c.Path)

	if IsProtect(pathfile) {
		c.Error(http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return nil
	}

	if IsIndex(pathfile) {
		return Index(c)
	}

	if strings.TrimSpace(c.Path) == "/" {
		return Index(c)
	}

	if IsCacheWork {
		return ProcessWithCache(c, pathfile)
	}

	return ProcessNoCache(c, pathfile)
}
