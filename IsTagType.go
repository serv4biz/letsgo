package letsgo

import (
	"strings"
)

// IsTagType check ext file has module tag render
func IsTagType(pathFile string) bool {
	for _, tagItem := range ListTagType {
		if strings.HasSuffix(pathFile, tagItem) {
			return true
		}
	}
	return false
}
