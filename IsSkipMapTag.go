package letsgo

import (
	"strings"
)

// IsSkipMapTag check path folder or file for skip tag render
func IsSkipMapTag(pathFile string) bool {
	for _, skipItem := range ListSkipMapTag {
		if strings.HasPrefix(pathFile, skipItem) {
			return true
		}
	}
	return false
}
