package letsgo

// SetMimeType is set header type for response
func SetMimeType(c *Context) {
	ext := GetPathExt(c.Path)
	mt, err := MimeType(ext)
	if err == nil {
		c.Response.Header().Set("Content-Type", mt)
	}
}
