package letsgo

import (
	"fmt"
	"strings"
)

func Translate(langCode string, message string) string {
	jsoLang, ok := JSOLangPacks.GetObject(strings.ToLower(langCode))
	if !ok {
		return message
	}

	msg := message
	for _, langTag := range ExtractLangTag(message) {
		if jsoLang.Check(langTag) {
			msg = strings.ReplaceAll(msg, fmt.Sprint("[-", langTag, "-]"), jsoLang.String(langTag))
		}
	}
	return msg
}
