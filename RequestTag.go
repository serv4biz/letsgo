package letsgo

import (
	"fmt"
	"strings"
)

func RequestTag(c *Context, content string) string {
	pathFile := fmt.Sprint(APP_DIR, c.Path)
	UseGet(c)

	data := strings.TrimSpace(content)
	if !IsSkipMapTag(pathFile) {
		for _, mapTag := range ExtractMapTag(data) {
			if c.GET.Check(mapTag) {
				data = strings.ReplaceAll(data, fmt.Sprint("{-", mapTag, "-}"), c.GET.String(mapTag))
			}
		}
	}

	if !IsSkipModTag(pathFile) {
		data = ModuleTagMap(c, data, c.GET)
	}

	if !IsSkipLangTag(pathFile) {
		Translate(c.Language, data)
	}
	return data
}
