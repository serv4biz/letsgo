package letsgo

import (
	"fmt"
	"strings"
)

// AddAPIFunc is add api function
func AddAPIFunc(pathAPI string, handler func(c *Context) error) {
	txtBaseRoute := ""
	for _, subItem := range strings.Split(fmt.Sprint("/", API_ROUTE, "/", strings.TrimSpace(strings.ToLower(pathAPI))), "/") {
		if subItem != "" {
			txtBaseRoute += "/" + subItem
		}
	}
	Route(txtBaseRoute, handler)
}
