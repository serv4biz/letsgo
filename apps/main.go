package main

import (
	"errors"
	"fmt"
	"io/fs"
	"os"

	"strings"

	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/letsgo"
	"gitlab.com/serv4biz/letsgo/apps/compilers"
)

func main() {
	// Compile configuration
	jsoCompileConfig, err := letsgo.GetConfig("letsgo.compile")
	if err != nil {
		logs.Panic(errors.New(fmt.Sprint("Can not load config letsgo.json file [ ", err, " ]")), true)
	}

	letsgo.API_DIR = "apis"
	letsgo.SOCKET_DIR = "sockets"
	letsgo.MODULE_DIR = "modules"

	letsgo.LANGUAGE_DIR = "languages"
	letsgo.TEMPLATE_DIR = "templates"
	letsgo.SESSION_DIR = "sessions"

	letsgo.API_ROUTE = "/api"
	letsgo.SOCKET_ROUTE = "/socket"

	letsgo.APP_MODULE = strings.Trim(jsoCompileConfig.String("txt_app_module"), "/")

	txtPath, ok := jsoCompileConfig.GetString("txt_api_path")
	if !ok {
		logs.Panic(errors.New("no txt_api_path key in json object"), true)
	}
	if txtPath != "" {
		letsgo.API_DIR = txtPath
	}

	txtPath, ok = jsoCompileConfig.GetString("txt_api_route")
	if !ok {
		logs.Panic(errors.New("no txt_api_route key in json object"), true)
	}
	if txtPath != "" {
		letsgo.API_ROUTE = txtPath
	}

	txtPath, ok = jsoCompileConfig.GetString("txt_socket_path")
	if !ok {
		logs.Panic(errors.New("no txt_socket_path key in json object"), true)
	}
	if txtPath != "" {
		letsgo.SOCKET_DIR = txtPath
	}

	txtPath, ok = jsoCompileConfig.GetString("txt_socket_route")
	if !ok {
		logs.Panic(errors.New("no txt_socket_route key in json object"), true)
	}
	if txtPath != "" {
		letsgo.SOCKET_ROUTE = txtPath
	}

	txtPath, ok = jsoCompileConfig.GetString("txt_module_path")
	if !ok {
		logs.Panic(errors.New("no txt_module_path key in json object"), true)
	}
	if txtPath != "" {
		letsgo.MODULE_DIR = txtPath
	}

	txtPath, ok = jsoCompileConfig.GetString("txt_language_path")
	if !ok {
		logs.Panic(errors.New("no txt_language_path key in json object"), true)
	}
	if txtPath != "" {
		letsgo.LANGUAGE_DIR = txtPath
	}

	txtPath, ok = jsoCompileConfig.GetString("txt_template_path")
	if !ok {
		logs.Panic(errors.New("no txt_template_path key in json object"), true)
	}
	if txtPath != "" {
		letsgo.TEMPLATE_DIR = txtPath
	}

	txtPath, ok = jsoCompileConfig.GetString("txt_session_path")
	if !ok {
		logs.Panic(errors.New("no txt_session_path key in json object"), true)
	}
	if txtPath != "" {
		letsgo.SESSION_DIR = txtPath
	}
	// End of compile configuration

	ProjectName := "LetsGo Compiler"
	ProjectVersion := "1.0.0"
	CompanyName := "SERV4BIZ CO.,LTD."

	pathAppDir, err := files.GetAppDir()
	logs.Panic(err, true)

	// Create languages
	pathLangDir := fmt.Sprint(pathAppDir, "/", letsgo.LANGUAGE_DIR)
	err = files.MakeDir(pathLangDir, fs.ModePerm)
	logs.Panic(err, true)

	fileNames, err := files.ScanDir(pathLangDir)
	logs.Panic(err, true)

	for _, val := range fileNames {
		fileName := strings.ToLower(val)
		if strings.HasSuffix(fileName, ".json") {
			langFile := fmt.Sprint(pathLangDir, "/", fileName)
			jsoLang, err := jsons.ObjectFromFile(langFile)
			logs.Panic(err, true)

			langCode := strings.TrimSuffix(fileName, ".json")
			letsgo.JSOLangPacks.PutObject(langCode, jsoLang)
		}
	}

	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println(fmt.Sprint(ProjectName, " Version ", ProjectVersion))
	fmt.Println(fmt.Sprint("Copyright © 2020 ", CompanyName, " All Rights Reserved."))
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println(fmt.Sprint("Directory : ", pathAppDir))
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")

	compilers.InitGenerate(letsgo.APP_MODULE)
	compilers.APIGenerate(letsgo.APP_MODULE)
	compilers.SocketGenerate(letsgo.APP_MODULE)
	compilers.ModuleGenerate(letsgo.APP_MODULE)

	letsgo.JSOLangPacks.EachObject(func(key string, lang *jsons.Object) error {
		langFile := fmt.Sprint(pathLangDir, "/", key, ".json")
		_, err := lang.ToFile(langFile)
		logs.Panic(err, true)
		return nil
	})

	fmt.Println("")
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println(fmt.Sprint(ProjectName, " Finished"))
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println("")

	if len(os.Args) > 1 {
		cmdName := os.Args[1]
		if strings.EqualFold(cmdName, "appmode") {
			fmt.Println("Run LetsGo In Application Mode")

			LetsGo()
			LetsGoAPI()
			LetsGoSocket()
			LetsGoModule()
			letsgo.Listen(0)
		}
	}
}
