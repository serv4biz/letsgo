package main

import (
	"gitlab.com/serv4biz/letsgo"
	sockets "gitlab.com/serv4biz/letsgo/apps/sockets"
)

func LetsGoSocket() {
	letsgo.AddSocketFunc("echo", sockets.Echo)

}
