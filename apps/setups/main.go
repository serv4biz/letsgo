package main

import (
	"fmt"

	"gitlab.com/serv4biz/letsgo"
)

func main() {
	fmt.Println("Letsgo Running")

	LetsGo()
	LetsGoAPI()
	LetsGoSocket()
	LetsGoModule()
	letsgo.Listen(0)
}
