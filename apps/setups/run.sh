#!/bin/bash

RUNLETSGO="letsgo.$1.$2"
RUNNAME="app.$1.$2"
PORT=$3

clear
./$RUNLETSGO
./build.sh

kill -9 $(lsof -t -i:$PORT)
./$RUNNAME $4