package networks

import (
	"net/http"

	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

// Ping is check network
func Ping(c *letsgo.Context) error {
	jsoResult := jsons.ObjectNew(0)
	jsoResult.PutInt("int_status", 1)
	return c.JSON(http.StatusOK, jsoResult)
}
