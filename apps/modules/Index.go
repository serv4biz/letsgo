package modules

import (
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/letsgo"
)

func Index(c *letsgo.Context, params *jsons.Object) string {
	c.Response.Header().Set("Content-Type", "text/html; charset=utf-8")
	return "<h1>Hello {{.PageTitle}} world</h1>"
}
