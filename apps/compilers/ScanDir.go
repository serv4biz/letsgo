package compilers

import (
	"fmt"
	"strings"

	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/letsgo"
)

// ScanDir is listing file in folder
func ScanDir(folder string, extName string, isScanLang bool) *jsons.Array {
	jsaResult := jsons.ArrayNew(0)
	filedirs, errScan := files.ScanDir(folder)
	if errScan == nil {
		for _, val := range filedirs {
			pathFile := fmt.Sprint(folder, "/", val)
			if files.IsFile(pathFile) {
				if strings.HasSuffix(strings.ToLower(val), fmt.Sprint(".", extName)) {
					jsaResult.PutString(pathFile)

					// Language Tag Scan
					buff, err := files.ReadFile(pathFile)
					logs.Panic(err, true)
					data := string(buff)

					if isScanLang {
						for _, val := range letsgo.ExtractLangTag(data) {
							letsgo.JSOLangPacks.EachObject(func(langCode string, lang *jsons.Object) error {
								if !lang.Check(val) {
									lang.PutString(val, val)
								}
								return nil
							})
						}
					} else {
						for _, str := range letsgo.ExtractStringTag(data) {
							for _, val := range letsgo.ExtractLangTag(str) {
								letsgo.JSOLangPacks.EachObject(func(langCode string, lang *jsons.Object) error {
									if !lang.Check(val) {
										lang.PutString(val, val)
									}
									return nil
								})
							}
						}
					}
				}
			} else {
				jsaList := ScanDir(pathFile, extName, isScanLang)
				for _, item := range *jsaList {
					jsaResult.PutAny(item)
				}
			}
		}
	}

	return jsaResult
}
