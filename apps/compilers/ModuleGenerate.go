package compilers

import (
	"fmt"
	"io/fs"
	"os/exec"
	"strings"

	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/letsgo"
)

// ModuleListing is listing api all in folder
func ModuleListing(folder string) *jsons.Array {
	pathAppDir, err := files.GetAppDir()
	logs.Panic(err, true)

	pathdir := fmt.Sprint(pathAppDir, "/", folder)

	jsaNList := jsons.ArrayNew(0)
	jsaList := ScanDir(pathdir, "go", false)
	jsaList.EachString(func(index int, value string) error {
		jsaNList.PutString(strings.TrimPrefix(fmt.Sprint("/", strings.Trim(strings.Replace(value, pathAppDir, "", 1), "/")), fmt.Sprint("/", letsgo.MODULE_DIR, "/")))
		return nil
	})
	return jsaNList
}

func ModuleGenerate(moduleName string) {
	pathAppDir, err := files.GetAppDir()
	logs.Panic(err, true)

	fmt.Println("")
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println("Generate Module All")
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println("")

	// Modules Imports
	jsaListing := ModuleListing(letsgo.MODULE_DIR)

	jsaPathAPI := jsons.ArrayNew(0)
	mapPathImport := make(map[string]string)
	jsaListing.EachString(func(index int, txtPathFile string) error {
		fmt.Println(txtPathFile)

		arrPaths := strings.Split(txtPathFile, "/")
		txtObjectName := "modules"
		txtImportPath := ""
		txtImportName := "modules"
		for _, val := range arrPaths {
			if strings.HasSuffix(val, ".go") {
				txtObjectName = fmt.Sprint(txtObjectName, ".", strings.TrimSuffix(val, ".go"))
				txtObjectName = strings.TrimPrefix(txtObjectName, "_")
				break
			} else {
				txtObjectName = fmt.Sprint(txtObjectName, "_", val)

				txtImportPath = fmt.Sprint(txtImportPath, "/", val)
				txtImportName = fmt.Sprint(txtImportName, "_", val)
			}
		}
		txtImportPath = strings.TrimPrefix(txtImportPath, "/")
		txtImportName = strings.TrimPrefix(txtImportName, "_")
		mapPathImport[txtImportPath] = txtImportName

		txtPathAPI := strings.TrimSuffix(txtPathFile, ".go")
		jsaPathAPI.PutString(fmt.Sprint("letsgo.AddModuleFunc(\"", strings.TrimSpace(strings.ToLower(txtPathAPI)), "\",", txtObjectName, ")"))
		return nil
	})

	txtImportBuffer := ""
	for keyName := range mapPathImport {
		pathPackage := fmt.Sprint(moduleName, "/", letsgo.MODULE_DIR, "/", keyName)
		pathPackage = strings.Trim(pathPackage, "/")
		txtImportBuffer = fmt.Sprint(txtImportBuffer, mapPathImport[keyName], " \"", pathPackage, "\"\n")
	}

	txtAddAPIBuffer := ""
	jsaPathAPI.EachString(func(index int, value string) error {
		txtAddAPIBuffer = fmt.Sprint(txtAddAPIBuffer, value, "\n")
		return nil
	})

	codeBuffer := `
	package main

	import(
		"gitlab.com/serv4biz/letsgo"
		` + txtImportBuffer + `
	)

	func LetsGoModule() {
		` + txtAddAPIBuffer + `
	}`

	pathFile := fmt.Sprint(pathAppDir, "/modules.go")
	if txtAddAPIBuffer == "" {
		files.DeleteFile(pathFile)
	} else {
		files.WriteFile(pathFile, []byte(codeBuffer), fs.ModePerm)

		cmd := exec.Command("go", "fmt", pathFile)
		cmd.Run()
	}
}
