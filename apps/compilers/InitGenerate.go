package compilers

import (
	"fmt"
	"io/fs"
	"os/exec"

	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/letsgo"
)

func InitGenerate(moduleName string) {
	pathAppDir, err := files.GetAppDir()
	logs.Panic(err, true)

	fmt.Println("")
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println("Generate Letsgo Initial")
	fmt.Println("Templates Scanning")
	fmt.Println("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * *")
	fmt.Println("")

	pathdir := fmt.Sprint(pathAppDir, "/", letsgo.TEMPLATE_DIR)
	list := ScanDir(pathdir, "html", true)
	list.EachString(func(index int, value string) error {
		fmt.Println(value)
		return nil
	})

	codeBuffer := `
	package main

	import(
		"gitlab.com/serv4biz/letsgo"
	)

	func LetsGo() {
		letsgo.APP_MODULE  = "` + letsgo.APP_MODULE + `"

		letsgo.API_DIR  = "` + letsgo.API_DIR + `"
		letsgo.API_ROUTE  = "` + letsgo.API_ROUTE + `"

		letsgo.SOCKET_DIR  = "` + letsgo.SOCKET_DIR + `"
		letsgo.SOCKET_ROUTE  = "` + letsgo.SOCKET_ROUTE + `"

		letsgo.MODULE_DIR  = "` + letsgo.MODULE_DIR + `"
		letsgo.LANGUAGE_DIR  = "` + letsgo.LANGUAGE_DIR + `"
		letsgo.TEMPLATE_DIR  = "` + letsgo.TEMPLATE_DIR + `"
		letsgo.SESSION_DIR  = "` + letsgo.SESSION_DIR + `"
	}`

	pathFile := fmt.Sprint(pathAppDir, "/letsgo.go")
	files.WriteFile(pathFile, []byte(codeBuffer), fs.ModePerm)

	cmd := exec.Command("go", "fmt", pathFile)
	cmd.Run()
}
