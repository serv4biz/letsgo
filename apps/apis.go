package main

import (
	"gitlab.com/serv4biz/letsgo"
	apis_networks "gitlab.com/serv4biz/letsgo/apps/apis/networks"
)

func LetsGoAPI() {
	letsgo.AddAPIFunc("networks/ping", apis_networks.Ping)

}
