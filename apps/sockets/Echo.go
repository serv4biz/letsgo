package price

import (
	"fmt"
	"time"

	"gitlab.com/serv4biz/letsgo"
	"golang.org/x/net/websocket"
)

func Echo(c *letsgo.Context, ws *websocket.Conn) error {
	fmt.Println("Open socket")
	defer ws.Close()

	var msg string
	err := websocket.Message.Receive(ws, &msg)
	if err != nil {
		fmt.Println("closed")
	}
	fmt.Println(msg)

	for {
		err := websocket.Message.Send(ws, "hello ,"+msg)
		if err != nil {
			fmt.Println("exit")
			break
		}
		<-time.After(time.Second * 3)
	}
	return nil
}
