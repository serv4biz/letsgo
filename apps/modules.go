package main

import (
	"gitlab.com/serv4biz/letsgo"
	modules "gitlab.com/serv4biz/letsgo/apps/modules"
	modules_head "gitlab.com/serv4biz/letsgo/apps/modules/head"
)

func LetsGoModule() {
	letsgo.AddModuleFunc("index", modules.Index)
	letsgo.AddModuleFunc("head/title", modules_head.Title)

}
