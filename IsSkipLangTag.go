package letsgo

import (
	"strings"
)

// IsSkipLangTag check path folder or file for skip tag render
func IsSkipLangTag(pathFile string) bool {
	for _, skipItem := range ListSkipLangTag {
		if strings.HasPrefix(pathFile, skipItem) {
			return true
		}
	}
	return false
}
