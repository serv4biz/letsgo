package letsgo

import (
	"net/http"
	"sync"

	"gitlab.com/serv4biz/gfp/jsons"
)

// API_DIR is path of folder apis
var API_DIR string = "apis"
var SOCKET_DIR string = "sockets"
var MODULE_DIR string = "modules"

var LANGUAGE_DIR string = "languages"
var TEMPLATE_DIR string = "templates"
var SESSION_DIR string = "sessions"

var API_ROUTE string = "/api"
var SOCKET_ROUTE string = "/socket"

var APP_MODULE string = ""
var APP_DIR string = "./"

// Cache is cache pack data
type Cache struct {
	Header http.Header
	Data   []byte
}

// Context is header pack data
type Context struct {
	Path      string
	SessionID string
	Language  string

	SESSION *jsons.Object
	GET     *jsons.Object
	POST    *jsons.Object
	FILE    *jsons.Array

	Response http.ResponseWriter
	Request  *http.Request
}

// SessionName is session name
var SessionName string = "LETSGO_SESSION_ID"

// DS is separator of directory
const DS string = "/"

// MaxRead is Max MultiPart of body in request
var MaxRead int = 0

// MaxSession is Max expire of session
var MaxSession int = 1800 // default 1800 second is 30 minute

// IsCacheWork is enable cache work
var IsCacheWork bool = false

var IsSessionWork bool = false
var IsDynamicWork bool = false
var IsStdTagWork bool = false

var ListIndex []string

// ListProtect is list security directory
var ListProtect []string

// ListTagType is list mine type for tag render
var ListTagType []string

// ListSkipTag is list path folder or file for skip tag render
var ListSkipTag []string

var ListSkipMapTag []string
var ListSkipLangTag []string
var ListSkipModTag []string

// MapCache is map for Cache
var MutexMapCache sync.RWMutex
var MapCache map[string]*Cache = make(map[string]*Cache)

// MapMimeType is map for mime type data
var MapMimeType map[string]string = make(map[string]string)
var MapModuleFunc map[string]func(c *Context, params *jsons.Object) string = make(map[string]func(c *Context, params *jsons.Object) string)

// Language
var JSOLanguage *jsons.Object = jsons.ObjectNew(0)
var JSOLangPacks *jsons.Object = jsons.ObjectNew(0)

// HTML Template
var JSOTemplatePacks *jsons.Object = jsons.ObjectNew(0)

// Header
var JSOHeader *jsons.Object = jsons.ObjectNew(0)
