package letsgo

import (
	"errors"
	"net/http"
)

// DeleteSession is delete session file
func DeleteSession(c *Context) error {
	if !IsSessionWork {
		return errors.New("session is not active")
	}

	cookie := &http.Cookie{
		Name:   SessionName,
		Value:  "",
		Path:   "/",
		MaxAge: -1,
	}
	http.SetCookie(c.Response, cookie)

	return DeleteSessionHandler.(func(c *Context) error)(c)
}
