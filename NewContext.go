package letsgo

import (
	"net/http"

	"gitlab.com/serv4biz/gfp/jsons"
)

func NewContext(w http.ResponseWriter, r *http.Request) *Context {
	if w != nil {
		r.Body = http.MaxBytesReader(w, r.Body, int64(MaxRead))
		JSOHeader.EachString(func(key, value string) error {
			w.Header().Set(key, value)
			return nil
		})
	}

	rep := new(Context)
	rep.Path = r.URL.Path
	rep.SessionID = ""
	rep.Language = JSOLanguage.String("txt_default")

	rep.SESSION = jsons.ObjectNew(0)
	rep.GET = jsons.ObjectNew(0)
	rep.POST = jsons.ObjectNew(0)
	rep.FILE = jsons.ArrayNew(0)

	rep.Response = w
	rep.Request = r
	return rep
}
