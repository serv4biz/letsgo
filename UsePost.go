package letsgo

import (
	"gitlab.com/serv4biz/gfp/jsons"
)

// UsePost is load post from for api
func UsePost(c *Context) error {
	err := c.Request.ParseForm()
	if err != nil {
		return err
	}

	for key, val := range c.Request.PostForm {
		if len(val) > 1 {
			list := jsons.ArrayNew(0)
			for _, sv := range val {
				list.PutString(sv)
			}
			c.POST.PutArray(key, list)
		} else {
			c.POST.PutString(key, val[0])
		}
	}
	return nil
}
