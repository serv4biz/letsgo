package letsgo

import (
	"strings"
)

// IsSkipModTag check path folder or file for skip tag render
func IsSkipModTag(pathFile string) bool {
	for _, skipItem := range ListSkipModTag {
		if strings.HasPrefix(pathFile, skipItem) {
			return true
		}
	}
	return false
}
