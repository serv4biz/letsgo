package letsgo

import (
	"strings"
)

// IsSkipTag check path folder or file for skip tag render
func IsSkipTag(pathFile string) bool {
	for _, skipItem := range ListSkipTag {
		if strings.HasPrefix(pathFile, skipItem) {
			return true
		}
	}
	return false
}
