package letsgo

import (
	"errors"
	"fmt"
	"io/fs"
	"log"
	"net/http"
	"os"
	"runtime"
	"strings"
	"time"

	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
)

func ScanTmpls(pathDir string) {
	fileNames, err := files.ScanDir(pathDir)
	logs.Panic(err, true)

	for _, val := range fileNames {
		fileName := strings.ToLower(val)
		tmplFile := fmt.Sprint(pathDir, "/", fileName)

		if files.IsFile(tmplFile) && strings.HasSuffix(fileName, ".html") {
			buffer, err := files.ReadFile(tmplFile)
			logs.Panic(err, true)

			txtPath := strings.Trim(strings.TrimPrefix(strings.TrimSuffix(tmplFile, ".html"), fmt.Sprint(APP_DIR, "/", TEMPLATE_DIR)), "/")
			JSOTemplatePacks.PutString(txtPath, string(buffer))
		} else if files.IsDir(tmplFile) {
			ScanTmpls(tmplFile)
		}
	}
}

// Listen is begin work
func Listen(port int) {
	var ok bool = false
	jsoConfig, err := GetConfig("letsgo")
	if err != nil {
		logs.Panic(errors.New(fmt.Sprint("can't load config letsgo.json file [ ", err, " ]")), true)
	}

	if port <= 0 {
		port, ok = jsoConfig.GetInt("int_port")
		if !ok {
			log.Panic("no int_port key in json object", true)
		}
	}

	IsSessionWork, ok = jsoConfig.GetBool("bln_session")
	if !ok {
		log.Panic("no bln_session key in json object", true)
	}

	SessionName, ok = jsoConfig.GetString("txt_session")
	if !ok {
		log.Panic("no txt_session key in json object", true)
	}

	if strings.TrimSpace(SessionName) == "" {
		IsSessionWork = false
	}

	IsCacheWork, ok = jsoConfig.GetBool("bln_cache")
	if !ok {
		log.Panic("no bln_cache key in json object", true)
	}

	IsDynamicWork, ok = jsoConfig.GetBool("bln_dynamic")
	if !ok {
		log.Panic("no bln_dynamic key in json object", true)
	}

	IsStdTagWork, ok = jsoConfig.GetBool("bln_stdtag")
	if !ok {
		log.Panic("no bln_stdtag key in json object", true)
	}

	jsoCryptoTLS, ok := jsoConfig.GetObject("jso_crypto_tls")
	if !ok {
		log.Panic("no jso_crypto_tls key in json object", true)
	}

	jsoMimetype, err := GetConfig("mimetype")
	if err != nil {
		logs.Panic(errors.New(fmt.Sprint("can't load config mimetype.json file [ ", err, " ]")), true)
	}

	exts := jsoMimetype.GetKeys()
	for _, extName := range exts {
		MapMimeType[extName] = strings.ToLower(jsoMimetype.String(extName))
	}

	pathDir, err := files.GetAppDir()
	logs.Panic(err, true)
	APP_DIR = pathDir

	jsaProtect, ok := jsoConfig.GetArray("jsa_protect")
	if !ok {
		logs.Panic(errors.New("no jsa_protect in json object"), true)
	}
	jsaProtect.EachString(func(index int, txtItem string) error {
		pathProtect := fmt.Sprint(APP_DIR, DS, strings.Trim(txtItem, DS))
		ListProtect = append(ListProtect, pathProtect)
		return nil
	})

	jsaIndex, ok := jsoConfig.GetArray("jsa_index")
	if !ok {
		logs.Panic(errors.New("no jsa_index in json object"), true)
	}
	jsaIndex.EachString(func(index int, txtItem string) error {
		pathIndex := fmt.Sprint(APP_DIR, DS, strings.Trim(txtItem, DS))
		ListIndex = append(ListIndex, pathIndex)
		return nil
	})

	jsaTagType, ok := jsoConfig.GetArray("jsa_tagtype")
	if !ok {
		logs.Panic(errors.New("no jsa_tagtype in json object"), true)
	}
	jsaTagType.EachString(func(index int, txtItem string) error {
		ListTagType = append(ListTagType, txtItem)
		return nil
	})

	jsoSkipTag, ok := jsoConfig.GetObject("jso_skiptag")
	if !ok {
		logs.Panic(errors.New("no jso_skiptag in json object"), true)
	}

	jsaSkipTag, ok := jsoSkipTag.GetArray("jsa_all")
	if !ok {
		logs.Panic(errors.New("no jsa_all in jso_skiptag json object"), true)
	}
	jsaSkipTag.EachString(func(index int, txtItem string) error {
		pathSkip := fmt.Sprint(APP_DIR, DS, strings.Trim(txtItem, DS))
		ListSkipTag = append(ListSkipTag, pathSkip)
		return nil
	})

	jsaSkipMapTag, ok := jsoSkipTag.GetArray("jsa_map")
	if !ok {
		logs.Panic(errors.New("no jsa_map in jso_skiptag json object"), true)
	}
	jsaSkipMapTag.EachString(func(index int, txtItem string) error {
		pathSkip := fmt.Sprint(APP_DIR, DS, strings.Trim(txtItem, DS))
		ListSkipMapTag = append(ListSkipMapTag, pathSkip)
		return nil
	})

	jsaSkipLangTag, ok := jsoSkipTag.GetArray("jsa_lang")
	if !ok {
		logs.Panic(errors.New("no jsa_lang in jso_skiptag json object"), true)
	}
	jsaSkipLangTag.EachString(func(index int, txtItem string) error {
		pathSkip := fmt.Sprint(APP_DIR, DS, strings.Trim(txtItem, DS))
		ListSkipLangTag = append(ListSkipLangTag, pathSkip)
		return nil
	})

	jsaSkipModTag, ok := jsoSkipTag.GetArray("jsa_mod")
	if !ok {
		logs.Panic(errors.New("no jsa_mod in jso_skiptag json object"), true)
	}
	jsaSkipModTag.EachString(func(index int, txtItem string) error {
		pathSkip := fmt.Sprint(APP_DIR, DS, strings.Trim(txtItem, DS))
		ListSkipModTag = append(ListSkipModTag, pathSkip)
		return nil
	})

	MaxRead, ok = jsoConfig.GetInt("int_max_read")
	if !ok {
		logs.Panic(errors.New("no int_max_read in json object"), true)
	}
	if MaxRead <= 0 {
		// Default max reader is 1024MB or 1GB
		MaxRead = 1024 * 1024 * 1024
	}

	maxSession, ok := jsoConfig.GetInt("int_max_session")
	if !ok {
		logs.Panic(errors.New("no int_max_session in json object"), true)
	}
	if maxSession > 0 {
		MaxSession = maxSession
	} else {
		// Default max session 1800 second is 30 minute
		MaxSession = 1800
	}

	pathSessDir := fmt.Sprint(APP_DIR, "/", SESSION_DIR)
	err = files.MakeDir(pathSessDir, fs.ModePerm)
	logs.Panic(err, true)

	// Create languages
	pathLangDir := fmt.Sprint(APP_DIR, "/", LANGUAGE_DIR)
	err = files.MakeDir(pathLangDir, fs.ModePerm)
	logs.Panic(err, true)

	fileNames, err := files.ScanDir(pathLangDir)
	logs.Panic(err, true)

	for _, val := range fileNames {
		fileName := strings.ToLower(val)
		if strings.HasSuffix(fileName, ".json") {
			langFile := fmt.Sprint(pathLangDir, "/", fileName)
			jsoLang, err := jsons.ObjectFromFile(langFile)
			logs.Panic(err, true)

			langCode := strings.TrimSuffix(fileName, ".json")
			JSOLangPacks.PutObject(langCode, jsoLang)
		}
	}

	JSOLanguage, ok = jsoConfig.GetObject("jso_language")
	if !ok {
		logs.Panic(errors.New("no jso_language in json object"), true)
	}

	// Header
	JSOHeader, ok = jsoConfig.GetObject("jso_header")
	if !ok {
		logs.Panic(errors.New("no jso_header in json object"), true)
	}

	// Load templates
	pathTmplDir := fmt.Sprint(APP_DIR, "/", TEMPLATE_DIR)
	if files.ExistFile(pathTmplDir) {
		ScanTmpls(pathTmplDir)
	}

	// Force GC to clear up
	go func() {
		for {
			<-time.After(time.Hour)

			if IsSessionWork {
				pathSessDir := fmt.Sprint(APP_DIR, "/", SESSION_DIR)
				fileNames, err := files.ScanDir(pathSessDir)
				if err == nil {
					for _, fileItem := range fileNames {
						pathFile := fmt.Sprint(pathSessDir, "/", fileItem)
						modStamp, err := files.ModifyStamp(pathFile)
						if err == nil {
							diffStamp := time.Now().UTC().Unix() - modStamp
							if diffStamp >= int64(MaxSession) {
								files.DeleteFile(pathFile)
							}
						}
					}
				}
			}

			runtime.GC()
		}
	}()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		ctx := NewContext(w, r)
		err := FilterHandler.(func(c *Context) error)(ctx)
		if logs.Error(err, true) {
			ctx.Error(http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		} else {
			err = Process(ctx)
			if logs.Error(err, true) {
				ctx.Error(http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
		}
	})

	certFile := fmt.Sprint(APP_DIR, DS, jsoCryptoTLS.String("txt_certfile"))
	keyFile := fmt.Sprint(APP_DIR, DS, jsoCryptoTLS.String("txt_keyfile"))

	if jsoCryptoTLS.Bool("bln_enable") {
		if files.ExistFile(certFile) && files.ExistFile(keyFile) {
			http.ListenAndServeTLS(fmt.Sprint("0.0.0.0:", port), certFile, keyFile, nil)
		} else {
			fmt.Println("Crypto SSL/TLS mode failed")
			fmt.Println("Certificate or Private Key not found")
			os.Exit(0)
		}
	} else {
		http.ListenAndServe(fmt.Sprint("0.0.0.0:", port), nil)
	}
}
