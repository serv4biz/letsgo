package letsgo

import (
	"strings"
)

// IsIndex check permision for path file
func IsIndex(pathFile string) bool {
	for _, indexItem := range ListIndex {
		if strings.HasPrefix(pathFile, indexItem) {
			return true
		}
	}
	return false
}
