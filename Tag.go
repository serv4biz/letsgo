package letsgo

import (
	"fmt"
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
)

func Tag(c *Context, content string, params *jsons.Object) string {
	data := strings.TrimSpace(content)
	for _, mapTag := range ExtractMapTag(data) {
		if params.Check(mapTag) {
			data = strings.ReplaceAll(data, fmt.Sprint("{-", mapTag, "-}"), params.String(mapTag))
		}
	}
	return Translate(c.Language, ModuleTagMap(c, data, params))
}
