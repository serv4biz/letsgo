package letsgo

import (
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
)

// ModuleTagRender is add tag function
func ModuleTagRender(c *Context, pathTag string, params *jsons.Object) string {
	tagCall, ok := MapModuleFunc[strings.TrimSpace(strings.ToLower(pathTag))]
	if !ok {
		return ""
	}
	return tagCall(c, params)
}
