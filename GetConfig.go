package letsgo

import (
	"fmt"
	"sync"

	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
)

// MutexMapConfig is mutex lock for MapConfig
var MutexMapConfig sync.RWMutex

// MapConfig is map for config object
var MapConfig *jsons.Object = jsons.ObjectNew(0)

// GetConfig is get config json object
func GetConfig(name string) (*jsons.Object, error) {
	var jsoConfig *jsons.Object = nil
	var errConfig error = nil

	MutexMapConfig.RLock()
	objConfig, ok := MapConfig.GetObject(name)
	if ok {
		jsoConfig, errConfig = objConfig.Clone()
	}
	MutexMapConfig.RUnlock()

	if logs.Error(errConfig, true) {
		return nil, errConfig
	}

	if jsoConfig != nil {
		return jsoConfig, nil
	}

	pathAppDir, err := files.GetAppDir()
	if logs.Error(err, true) {
		return nil, err
	}

	MutexMapConfig.Lock()
	pathfile := fmt.Sprint(pathAppDir, DS, "configs", DS, name, ".json")
	jsoConfig, errConfig = jsons.ObjectFromFile(pathfile)
	if errConfig == nil {
		MapConfig.SetObject(name, jsoConfig)
	}
	MutexMapConfig.Unlock()

	if logs.Error(errConfig, true) {
		return nil, errConfig
	}
	return jsoConfig.Clone()
}
