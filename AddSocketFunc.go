package letsgo

import (
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/serv4biz/gfp/logs"
	"golang.org/x/net/websocket"
)

// AddSocketFunc is add socket function
func AddSocketFunc(pathSocket string, handler func(c *Context, ws *websocket.Conn) error) {
	txtBaseRoute := ""
	for _, subItem := range strings.Split(fmt.Sprint("/", SOCKET_ROUTE, "/", strings.TrimSpace(strings.ToLower(pathSocket))), "/") {
		if subItem != "" {
			txtBaseRoute += "/" + subItem
		}
	}
	http.Handle(txtBaseRoute, websocket.Handler(func(ws *websocket.Conn) {
		ctx := NewContext(nil, ws.Request())
		err := FilterHandler.(func(c *Context) error)(ctx)
		if logs.Error(err, true) {
			ctx.Error(http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		} else {
			err = handler(ctx, ws)
			if logs.Error(err, true) {
				ctx.Error(http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
		}
	}))
}
