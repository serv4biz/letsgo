module gitlab.com/serv4biz/letsgo

go 1.24

replace gitlab.com/serv4biz/letsgo => ./

require (
	gitlab.com/serv4biz/gfp v1.2.6
	golang.org/x/net v0.35.0
)

require github.com/oklog/ulid/v2 v2.1.0 // indirect
