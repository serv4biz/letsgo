package letsgo

import (
	"bytes"
	"html/template"

	"gitlab.com/serv4biz/gfp/jsons"
)

func StdTagRender(c *Context, buffer string, data *jsons.Object) (string, error) {
	if !IsStdTagWork {
		return buffer, nil
	}

	tmpl, err := template.New(c.Path).Parse(buffer)
	if err != nil {
		return "", err
	}

	var tmplBuff bytes.Buffer
	err = tmpl.Execute(&tmplBuff, data)
	if err != nil {
		return "", err
	}
	return tmplBuff.String(), nil
}
