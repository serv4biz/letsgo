package letsgo

import (
	"errors"
)

// MimeType is set header type for response
func MimeType(txtExt string) (string, error) {
	mimetype, ok := MapMimeType[txtExt]
	if !ok {
		return "", errors.New("mime type not support")
	}
	return mimetype, nil
}
