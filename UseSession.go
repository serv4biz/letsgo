package letsgo

import (
	"errors"
	"net/http"
)

// UseSession is load session first
func UseSession(c *Context) (string, error) {
	if !IsSessionWork {
		return "", errors.New("session is not active")
	}

	cookie, err := c.Request.Cookie(SessionName)
	if err != nil {
		err = SessionIDHandler.(func(c *Context) error)(c)
		if err != nil {
			return "", err
		}
		cookie = &http.Cookie{Name: SessionName, Value: c.SessionID, MaxAge: MaxSession, Path: "/"}
		http.SetCookie(c.Response, cookie)
	}
	c.SessionID = cookie.Value
	err = LoadSessionHandler.(func(c *Context) error)(c)
	if err != nil {
		return "", err
	}

	return c.SessionID, nil
}
