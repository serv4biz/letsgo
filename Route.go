package letsgo

import (
	"net/http"

	"gitlab.com/serv4biz/gfp/logs"
)

// Route is add handle func
func Route(path string, handler func(c *Context) error) {
	http.HandleFunc(path, func(w http.ResponseWriter, r *http.Request) {
		ctx := NewContext(w, r)
		err := FilterHandler.(func(c *Context) error)(ctx)
		if logs.Error(err, true) {
			ctx.Error(http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		} else {
			err := handler(ctx)
			if logs.Error(err, true) {
				ctx.Error(http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
		}
	})
}
