package letsgo

import (
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
)

func Template(c *Context, pathTmpl string, params *jsons.Object) string {
	data := JSOTemplatePacks.String(pathTmpl)
	buffer := Tag(c, data, params)

	var err error = nil
	buffer, err = StdTagRender(c, buffer, params)
	if err != nil {
		buffer = logs.Message(err)
	}
	return buffer
}
