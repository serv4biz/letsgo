package letsgo

import (
	"fmt"
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
)

// UseGet is load parameter for api path
func UseGet(c *Context) {
	path := strings.TrimSuffix(strings.ToLower(c.Path), ".html")
	params := strings.Split(path, DS)

	txtLang := JSOLanguage.String("txt_default")
	tagName := JSOLanguage.String("txt_tagname")
	c.GET.PutString(tagName, txtLang)

	pathapi := ""
	count := 0
	for index, val := range params {
		if index >= 2 {
			pathapi = fmt.Sprint(pathapi, "/", val)
			c.GET.PutString(fmt.Sprint("var", count), val)
			count++
		}
	}

	pathapi = strings.Trim(pathapi, "/")
	c.GET.PutString("path", pathapi)

	// Check parameter
	for key, val := range c.Request.URL.Query() {
		if len(val) > 1 {
			list := jsons.ArrayNew(0)
			for _, sv := range val {
				list.PutString(sv)
			}
			c.GET.PutArray(key, list)
		} else {
			c.GET.PutString(key, val[0])
		}
	}
}
