package letsgo

import (
	"fmt"
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
)

// ModuleTagMap is change tag in file to system info
func ModuleTagMap(c *Context, buffer string, jsoParams *jsons.Object) string {
	arrAllTag := ExtractModuleTag(buffer)
	if len(arrAllTag) == 0 {
		return buffer
	}

	nbuff := strings.TrimSpace(buffer)
	for _, moduleTag := range arrAllTag {
		resultTag := ModuleTagMap(c, moduleTag, jsoParams)

		mods := strings.SplitN(resultTag, ":", 2)
		pathModule := mods[0]
		params := jsons.ObjectNew(0)

		if len(mods) > 1 {
			if mods[1] != "" {
				var err error = nil
				params, err = jsons.ObjectFromString(mods[1])
				if err != nil {
					params = jsons.ObjectNew(0)
				}
			}
		}
		nbuff = strings.ReplaceAll(nbuff, fmt.Sprint("<?=", moduleTag, "?>"), ModuleTagRender(c, pathModule, params))
	}
	return nbuff
}
