package letsgo

import (
	"strings"
)

// IsProtect check permision for path file
func IsProtect(pathFile string) bool {
	for _, protectItem := range ListProtect {
		if strings.HasPrefix(pathFile, protectItem) {
			return true
		}
	}
	return false
}
