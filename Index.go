package letsgo

import (
	"fmt"
	"net/http"
	"os"

	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/logs"
)

// Index is main page
func Index(c *Context) error {
	if IsDynamicWork {
		c.UseGet()
		_, err := fmt.Fprint(c.Response, c.Module("Index", c.GET))
		if logs.Error(err, true) {
			c.Error(http.StatusText(http.StatusNotFound), http.StatusNotFound)
			return err
		}
	} else {
		pathfile := fmt.Sprint(APP_DIR, "/index.html")
		if !files.ExistFile(pathfile) {
			c.Error(http.StatusText(http.StatusNotFound), http.StatusNotFound)
			return os.ErrNotExist
		}

		c.Response.Header().Set("Content-Type", "text/html; charset=utf-8")
		buff, err := files.ReadFile(pathfile)
		if logs.Error(err, true) {
			return err
		}

		if IsTagType(pathfile) && !IsSkipTag(pathfile) {
			buff = []byte(RequestTag(c, string(buff)))
		}
		c.Response.Write(buff)
	}
	return nil
}
