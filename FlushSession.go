package letsgo

import "errors"

// FlushSession is save session to file
func FlushSession(c *Context) error {
	if !IsSessionWork {
		return errors.New("session is not active")
	}

	return SaveSessionHandler.(func(c *Context) error)(c)
}
