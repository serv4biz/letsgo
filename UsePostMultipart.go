package letsgo

import (
	"gitlab.com/serv4biz/gfp/jsons"
)

// UsePostMultipart is load post multipart form for api
func UsePostMultipart(c *Context) error {
	err := c.Request.ParseMultipartForm(int64(MaxRead))
	if err != nil {
		return err
	}

	for key, val := range c.Request.PostForm {
		if len(val) > 1 {
			list := jsons.ArrayNew(len(val))
			for _, sv := range val {
				list.PutString(sv)
			}
			c.POST.PutArray(key, list)
		} else {
			c.POST.PutString(key, val[0])
		}
	}
	for key := range c.Request.MultipartForm.File {
		c.FILE.PutString(key)
	}
	return nil
}
