package letsgo

import (
	"fmt"

	"gitlab.com/serv4biz/gfp/files"
	"gitlab.com/serv4biz/gfp/jsons"
	"gitlab.com/serv4biz/gfp/logs"
	"gitlab.com/serv4biz/gfp/ulids"
)

// SessionIDHandler is custom session id for all request
var SessionIDHandler any = func(c *Context) error {
	myulid, err := ulids.New()
	if logs.Error(err, true) {
		return err
	}
	c.SessionID = myulid.String()
	return nil
}

// LoadSessionHandler is start session of all request
var LoadSessionHandler any = func(c *Context) error {
	filename := fmt.Sprint(c.SessionID, ".json")
	pathfile := fmt.Sprint(APP_DIR, DS, SESSION_DIR, DS, filename)

	if !files.ExistFile(pathfile) {
		objNewFile := jsons.ObjectNew(0)
		_, err := objNewFile.ToFile(pathfile)
		if err != nil {
			return err
		}
	}

	var err error = nil
	c.SESSION, err = jsons.ObjectFromFile(pathfile)
	return err
}

// SaveSessionHandler is flush session of all request
var SaveSessionHandler any = func(c *Context) error {
	filename := fmt.Sprint(c.SessionID, ".json")
	pathfile := fmt.Sprint(APP_DIR, DS, SESSION_DIR, DS, filename)

	_, err := c.SESSION.ToFile(pathfile)
	return err
}

// DeleteSessionHandler is flush session of all request
var DeleteSessionHandler any = func(c *Context) error {
	filename := fmt.Sprint(c.SessionID, ".json")
	pathfile := fmt.Sprint(APP_DIR, DS, SESSION_DIR, DS, filename)
	return files.DeleteFile(pathfile)
}

var FilterHandler any = func(c *Context) error {
	return nil
}
