package letsgo

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"

	"gitlab.com/serv4biz/gfp/jsons"
)

// Module is module tag render
func (me *Context) Module(pathModule string, jsoParams *jsons.Object) string {
	tagCall, ok := MapModuleFunc[strings.TrimSpace(strings.ToLower(pathModule))]
	if ok {
		return tagCall(me, jsoParams)
	}
	return fmt.Sprint("not found module : ", pathModule)
}

// Template is module tag render
func (me *Context) Template(pathTmpl string, jsoParams *jsons.Object) string {
	return Template(me, pathTmpl, jsoParams)
}

func (me *Context) Tag(content string, jsoParams *jsons.Object) string {
	return Tag(me, content, jsoParams)
}

func (me *Context) Translate(message string) string {
	return Translate(me.Language, message)
}

// ReadFile is read file from multipart request
func (me *Context) ReadFile(keyname string) (*jsons.Object, []byte, error) {
	file, header, err := me.Request.FormFile(keyname)
	if err != nil {
		return nil, nil, err
	}
	defer file.Close()

	txtExt := "dat"
	exts := strings.Split(header.Filename, ".")
	if len(exts) >= 2 {
		txtExt = exts[len(exts)-1]
	}

	jsoInfo := jsons.ObjectNew(0)
	jsoInfo.PutString("txt_name", header.Filename)
	jsoInfo.PutInt("int_size", int(header.Size))
	jsoInfo.PutString("txt_ext", txtExt)

	buffer, err := io.ReadAll(file)
	if err != nil {
		return nil, nil, err
	}

	return jsoInfo, buffer, nil
}

func (me *Context) Body() ([]byte, error) {
	buff, err := io.ReadAll(me.Request.Body)
	if err != nil {
		return nil, err
	}
	defer me.Request.Body.Close()

	return buff, nil
}

func (me *Context) BodyString() (string, error) {
	buff, err := me.Body()
	if err != nil {
		return "", err
	}
	return string(buff), nil
}

func (me *Context) BodyObject() (*jsons.Object, error) {
	buff, err := me.BodyString()
	if err != nil {
		return nil, err
	}
	return jsons.ObjectFromString(buff)
}

func (me *Context) BodyArray() (*jsons.Array, error) {
	buff, err := me.BodyString()
	if err != nil {
		return nil, err
	}
	return jsons.ArrayFromString(buff)
}

func (me *Context) Error(statusText string, statusCode int) {
	http.Error(me.Response, statusText, statusCode)
}

func (me *Context) Write(statusCode int, buffers []byte) error {
	me.Response.WriteHeader(statusCode)
	_, err := me.Response.Write(buffers)
	return err
}

func (me *Context) WriteString(statusCode int, message string) error {
	return me.Write(statusCode, []byte(message))
}

func (me *Context) HTML(statusCode int, data string) error {
	me.Response.Header().Set("Content-Type", "text/html; charset=utf-8")
	return me.WriteString(statusCode, data)
}

func (me *Context) JSON(statusCode int, data any) error {
	me.Response.Header().Set("Content-Type", "application/json")

	switch data := data.(type) {
	case map[string]any:
		buff, err := json.Marshal(data)
		if err != nil {
			return err
		}
		return me.Write(statusCode, buff)
	case *map[string]any:
		buff, err := json.Marshal(data)
		if err != nil {
			return err
		}
		return me.Write(statusCode, buff)
	case jsons.Object:
		buff, err := data.ToString()
		if err != nil {
			return err
		}
		return me.WriteString(statusCode, buff)
	case *jsons.Object:
		buff, err := data.ToString()
		if err != nil {
			return err
		}
		return me.WriteString(statusCode, buff)
	case []any:
		buff, err := json.Marshal(data)
		if err != nil {
			return err
		}
		return me.Write(statusCode, buff)
	case *[]any:
		buff, err := json.Marshal(data)
		if err != nil {
			return err
		}
		return me.Write(statusCode, buff)
	case jsons.Array:
		buff, err := data.ToString()
		if err != nil {
			return err
		}
		return me.WriteString(statusCode, buff)
	case *jsons.Array:
		buff, err := data.ToString()
		if err != nil {
			return err
		}
		return me.WriteString(statusCode, buff)
	}
	return errors.New("json type is invalid")
}

func (me *Context) UseGet() {
	UseGet(me)
}

func (me *Context) UsePost() error {
	return UsePost(me)
}

func (me *Context) UsePostMultipart() error {
	return UsePostMultipart(me)
}

func (me *Context) UseSession() (string, error) {
	return UseSession(me)
}
